import click
from time import sleep
from .basic import writeAddress, readChannelAddress, writeChannelAddress


STATUS_MAP = {0: "ERROR", 1: "UNLOCKED", 2: "LOCKING", 4: "LOCKED"}


def resetHealthMonitoring(hw, id=0):
    writeAddress(hw, "payload.fe.chan_sel", id)
    writeAddress(hw, "payload.fe_chan.fe_ctrl.counter_reset", 1)
    sleep(0.1)
    writeAddress(hw, "payload.fe_chan.fe_ctrl.counter_reset", 0)
    sleep(0.1)


def statusColor(status):
    if status == 4:
      status_color = "green"
    elif status == 2 or status == 1:
      status_color = "yellow"
    else:
      status_color = "red"
    return status_color


def resetDTCAligner(hwdevice, channel=0):
    sleep_time = 0.1
    writeAddress(hwdevice, "payload.fe.chan_sel", channel)
    sleep(sleep_time)
    status = [0, 0]
    status_color = ["", ""]
    for i in range(2):
        writeChannelAddress(
            hwdevice, 
            ("payload.fe.chan_sel", channel), 
            ("payload.fe_chan.fe_ctrl.aligner_reset{}".format(i), 1)
        )
        sleep(sleep_time)
    for i in range(2):
        status[i] = readChannelAddress(
            hwdevice,
            ("payload.fe.chan_sel", channel),
            ("payload.fe_chan.health_mon.aligner_state{}".format(i),)
        ).value()
        status_color[i] = statusColor(status[i])
        sleep(sleep_time)
    click.echo("CIC 0: {:<20} | CIC 1: {:<20}".format(
        click.style(STATUS_MAP[status[0]], fg=status_color[0]),
        click.style(STATUS_MAP[status[1]], fg=status_color[1])
    ))
    for i in range(2):
        writeChannelAddress(
            hwdevice, 
            ("payload.fe.chan_sel", channel), 
            ("payload.fe_chan.fe_ctrl.aligner_reset{}".format(i), 0)
        )
        sleep(sleep_time)
    for i in range(2):
        status[i] = readChannelAddress(
            hwdevice,
            ("payload.fe.chan_sel", channel),
            ("payload.fe_chan.health_mon.aligner_state{}".format(i),)
        ).value()
        status_color[i] = statusColor(status[i])
        sleep(sleep_time)
    click.echo("CIC 0: {:<20} | CIC 1: {:<20}".format(
        click.style(STATUS_MAP[status[0]], fg=status_color[0]),
        click.style(STATUS_MAP[status[1]], fg=status_color[1])
    ))

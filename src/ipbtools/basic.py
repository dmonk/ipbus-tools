import logging
import uhal


def getDeviceHandler(package, device):
    manager = uhal.ConnectionManager("file://{}/connections.xml".format(package))
    return manager.getDevice(device) 


def readAddress(device, address):
    data = device.getNode(address).read()
    device.dispatch()
    return data


def writeAddress(device, address, value):
    device.getNode(address).write(value)
    device.dispatch()
    read_value = readAddress(device, address)
    assert read_value == value
    logging.debug(f"{address}: 0x{read_value:x}")


def readMemory(manager, address, width):
    data = manager.getNode(address).readBlock(int(2**width))
    manager.dispatch()
    return data


def readChannelAddress(device, channel, address, safe=True):
    data = 0
    try:
        device.getNode(channel[0]).write(channel[1])
        logging.debug(f"{channel[0]}: 0x{channel[1]:x}")
        data = device.getNode(address[0]).read()
        device.dispatch()
        logging.debug(f"{address[0]}: 0x{data:x}")
    except uhal._core.exception as e:
        if safe:
            raise e
        else:
            logging.error(e)
    finally:
        return data
    

def readChannelMemory(device, channel, address, width, safe=True):
    data = 0
    try:
        device.getNode(channel[0]).write(channel[1])
        logging.debug(f"{channel[0]}: 0x{channel[1]:x}")
        data = device.getNode(address[0]).readBlock(int(2**width))
        device.dispatch()
    except uhal._core.exception as e:
        logging.error(e)
        if safe:
            raise e
        else:
            logging.error(e)
    finally:
        return data


def writeChannelAddress(device, channel, address, safe=True):
    try:
        device.getNode(channel[0]).write(channel[1])
        device.getNode(address[0]).write(address[1])
        device.dispatch()
        read_value = readChannelAddress(device, channel, address)
        assert read_value == address[1]
    except uhal._core.exception as e:
        if safe:
            raise e
        else:
            logging.error(e)
        

def readQuadChannelAddress(device, quad, channel, address, safe=True):
    data = 0
    try:
        device.getNode(quad[0]).write(quad[1])
        logging.debug(f"{quad[0]}: 0x{quad[1]:x}")
        device.getNode(channel[0]).write(channel[1])
        logging.debug(f"{channel[0]}: 0x{channel[1]:x}")
        data = device.getNode(address[0]).read()
        device.dispatch()
        logging.debug(f"{address[0]}: 0x{data:x}")
    except uhal._core.exception as e:
        if safe:
            raise e
        else:
            logging.error(e)
    finally:
        return data


def writeQuadChannelAddress(device, quad, channel, address, safe=True):
    try:
        device.getNode(quad[0]).write(quad[1])
        device.getNode(channel[0]).write(channel[1])
        device.getNode(address[0]).write(address[1])
        device.dispatch()
        read_value = readQuadChannelAddress(device, quad, channel, address)
        assert read_value == address[1]
    except uhal._core.exception as e:
        if safe:
            raise e
        else:
            logging.error(e)
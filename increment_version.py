import tomlkit
import click


@click.command()
@click.argument("previous")
@click.argument("filename")
@click.option("-p", "--patch", is_flag=True)
@click.option("-m", "--minor", is_flag=True)
@click.option("-M", "--major", is_flag=True)
def main(previous, filename, patch, minor, major):
    with open(filename, "r+") as f:
        config = tomlkit.parse(f.read())
        version_string = previous
        major_num, minor_num, patch_num = [int(i) for i in version_string.split(".")]
        if patch:
            patch_num += 1
        if minor:
            minor_num += 1
            patch_num = 0
        if major:
            major_num += 1
            minor_num = 0
            patch_num = 0
        config["project"]["version"] = ".".join([str(major_num), str(minor_num), str(patch_num)])
        click.echo(config["project"]["version"])
        f.seek(0)
        f.write(tomlkit.dumps(config)+"\n")


if __name__ == "__main__":
    main()